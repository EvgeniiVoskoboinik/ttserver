var express = require('express')
    , path = require('path');

require('lib/mongoose');

var HttpError = require('error').HttpError
    , cookieParser = require('cookie-parser')
    , bodyParser = require('body-parser')
    , favicon = require('serve-favicon');

var app = express();

app.engine('ejs', require('ejs-locals'));
app.set('views', path.join(__dirname, '/template'));
app.set('view engine', 'ejs');

app.use(favicon(path.join(__dirname, 'public','images','favicon.ico')));

app.use(cookieParser())
    .use(bodyParser.urlencoded({ extended: false }))
    .use(bodyParser.json());

app.use(require('middleware/sendHttpError'));

require('routes')(app);

app.use("/public", express.static(__dirname + '/public'));

app.use(function(req, res){
    res.sendHttpError(new HttpError(404));
});

app.use(function(err, req, res) {
    if (typeof err == 'number') {
        err = new HttpError(err);
    }
    if (err instanceof HttpError) {
        res.sendHttpError(err);
    }
    else {
        err = new HttpError(500);
        res.sendHttpError(err);
    }
});

var server = require('http').Server(app).listen(process.env.PORT);
var io = require('./socket')(server);