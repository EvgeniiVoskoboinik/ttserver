var mongoose = require('mongoose'),
    config = require('config');

mongoose.connect(process.env.MONGO_URL, config.get('mongoose.options'));

module.exports = mongoose;
