var sessionStore = require('lib/sessionStore'),
    config = require('config'),
    session = require('express-session')({
        saveUninitialized: true,
        resave: true,
        secret: config.get('session.secret'),
        key: config.get('session.key'),
        cookie: config.get('session.cookie'),
        //domain: 'ttyper.com',
        store: sessionStore
    });

module.exports = session;