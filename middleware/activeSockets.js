function SList (){
    var self = this,
        socketsList = {};

    self.addSocket = function(socket,ip, username, sessionId){
        socketsList[socket] = [ip, username, sessionId];
    };

    self.deleteSocket = function(socket){
        delete socketsList[socket];
    };

    self.getSocketsList = function(){
        return socketsList;
    };

    self.getSessionId = function(socket){
        return socketsList[socket][2];
    };
}

var activeSockets = new SList();

module.exports = activeSockets;