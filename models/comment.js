var mongoose = require('lib/mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    room:{
        type: String,
        required: true
    },
    user_id:{
        type: String,
        required: true
    },
    from:{
        type: String,
        required: true
    },
    text:{
        type: String,
        required: true
    },
    response_to: {
        type: String
    },
    time:{
        type: Date,
        default: Date.now
    }
});

schema.statics.saveComment = function(room, user_id, from, text, response_to, callback) {
    if(response_to != null){
        try{
            response_to = mongoose.Types.ObjectId(response_to);
        } catch (err){
            callback('Неверный res_to');
        }
    }

    var Comment = this;
    var comment = new Comment({
        room: room,
        user_id: user_id,
        from: from,
        text: text,
        response_to:response_to
    });

    comment.save(function(err) {
        if (err)
            return callback(err);
        return callback(null, comment);
    });
};

schema.statics.getCommentbyId = function(id, callback){
    try{
        id = mongoose.Types.ObjectId(id);
    } catch (err){
        callback('Неверный id');
    }

    var Comment = this;

    Comment
        .find({_id: id})
        .select('from text time')
        .exec(function(err, comment){
            if (err)
                return callback(err);
            return callback (null, comment);
        });
};


schema.statics.getComments = function(room, id, newer, callback) {

    try{
        id = mongoose.Types.ObjectId(id);
    } catch (err){
        callback('Неверный id');
    }

    var limit = 30;

    if(newer){
        var compare = {$gt: id};

        if (id != '000000000000000000000000')
            limit = 0;
    }
    else
        compare = {$lt: id};

    var Comment = this;

    Comment
        .find({room: room})
        .where({_id: compare})
        .sort({time: -1})
        .limit(limit)
        .select('_id from text response_to time')
        .exec(function(err, comments){
            if (err)
                return callback(err);
            return callback (null, comments);
        });
};

schema.statics.getAllComments = function(callback) {
    var Comment = this;
    Comment
        .find({})
        .sort('time')
        .limit(100)
        .exec(function(err, comments){
            if (err) {
                return callback(err);
            }
            return callback (null, comments);
        });
};

schema.statics.removeComments = function(ids, callback) {
    var Comment = this;
    Comment.remove({_id: {$in:ids}}, function(err){
        if (err) return callback (err);
        return callback();
    });
};

exports.Comment = mongoose.model('Comment', schema);