var crypto = require('crypto');
var async = require('async');

var mongoose = require('lib/mongoose'),
  Schema = mongoose.Schema;

var schema = new Schema({
  username: {
    type: String,
    unique: true,
    required: true
  },
  hashedPassword: {
    type: String,
    required: true
  },
  salt: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  },
    status: {
        type: Number,
        default: 0
    }
});

schema.methods.encryptPassword = function(password) {
    var hmac = crypto.createHmac('sha1', this.salt);
    hmac.setEncoding('hex');
    hmac.write(password);
    hmac.end();
    return hmac.read();
  //return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

schema.virtual('password')
  .set(function(password) {
    this._plainPassword = password;
    this.salt = Math.random() + '';
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function() { return this._plainPassword; });


schema.methods.checkPassword = function(password) {
  return this.encryptPassword(password) === this.hashedPassword;
};

schema.statics.authorize = function(username, password, callback) {
  var User = this;
  async.waterfall([
    function(callback) {
      User.findOne({username: username}, callback);
    },
    function(user, callback) {
      if (user) {
        if (user.checkPassword(password)) {
          callback(null, user);
        } else {
          callback("Неправильный пароль");
        }
      } else {
          callback("Пользователь с таким именем не найден");
      }
    }
  ], callback);
};

schema.statics.registration = function(username, password, callback) {
    var User = this;

    async.waterfall([
        function(callback) {
            User.findOne({username: username}, callback);
        },
        function(user, callback) {
            if (user) {
              callback("Пользователь с таким именем уже существует");

            } else {
                var new_user = new User({username: username, password: password});
                new_user.save(function cb(err) {
                    if (err) return callback(err);
                    return callback(null, new_user);
                });
            }
        }
    ], callback());
};

schema.statics.getAllUsers = function(callback) {
    var User = this;

    User
        .find({})
        .sort({created: 1})
        .limit(100)
        .exec(function(err, users){
            if (err) return callback (err);
            return callback(null, users);
        });
};

schema.statics.removeUsers = function(ids, callback) {
  var User = this;
  User.remove({_id: {$in:ids}}, function(err){
      if (err) return callback (err);
      return callback();
  });
};

schema.statics.getUser = function(uid, callback) {
    try{
        uid = mongoose.Types.ObjectId(uid);
    } catch (err){
        callback('Неверный uid');
    }

    var User = this;
    User.findOne({_id: uid}, function(err, user){
        if (err) return callback (err);
        return callback(null, user);
    });
};

exports.User = mongoose.model('User', schema);