$(function(){
    var utbody = $('#utbody');
    var ctbody = $('#ctbody');
    var stbody = $('#stbody');
    var allComments = [];

    $("#utbody, #ctbody, #stbody").selectable()
    .selectable("option", "autoRefresh", false);


    var socket = io.connect('/');
    socket
        .on('connect', function(){
            socket.emit('admin mode on');
        })

        .on('comment', function(data){
            newComment(data);
        })

        .on('disconnect', function(){
            socket.emit('admin mode off');
        })

        .on('user reg', function(data){
            newUser(data);
        })

        .on('addSocketUsername', function(data){
            addSocketUsername(data.sid, data.username);
        })

        .on('getSockets', function(data){
            parseSockets(data);
        })

        .on('deleteSocket', function(data){
            $('#'+data.sid).remove();
        });

    function parseSockets(sockets){
        if (sockets != {}){
            for(var sid in sockets){
                var ip = sockets[sid][0];
                var username = sockets[sid][1];
                createSBlock(sid, sockets[sid][0], sockets[sid][1]);
            }
            stbody.selectable("refresh");
        }
    }

    function createSBlock(sid, ip, username){

        var tr = document.createElement('tr');
        var sId = document.createElement('td');
        var sIp = document.createElement('td');
        var sUsername = document.createElement('td');

        sId.setAttribute("class", "id");
        sId.setAttribute("style", "display:none");
        sUsername.setAttribute("class", "sName");
        tr.setAttribute('id', sid);

        sId.innerText = sid;
        sIp.innerText = ip;
        sUsername.innerText = username;

        tr.appendChild(sId);
        tr.appendChild(sIp);
        tr.appendChild(sUsername);
        stbody[0].appendChild(tr);
    }

    function addSocketUsername(sid, username){
        $('#'+sid).find('.sName').text(username);
    }

    function newUser(user){
        createUBlock(user._id, user.username, user.created);
        utbody.selectable("refresh");
    }

    function newComment(comment){
        createCBlock(comment._id, comment.room, comment.user_id, comment.from, comment.text, comment.time);
        ctbody.selectable("refresh");
    }

    function makePostRequest(data,  callback){
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "/admin", true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if(xhr.status == 200) {
                    callback(null, JSON.parse(xhr.responseText));
                }
                else callback(xhr.status);
            }
        };
        xhr.send(JSON.stringify(data));
    }

    function parseUsers(users){
        if (users != null){
            for(var i=0; i<users.length; i++){
                var user = users[i];
                createUBlock(user._id, user.username, user.created);
            }
            utbody.selectable("refresh");
        }
    }

    function parseComments(comments){
        if (comments != null){
            for(var i=0; i<comments.length; i++){
                var comment = comments[i];
                createCBlock(comment._id, comment.room, comment.user_id, comment.from, comment.text, comment.time);
            }
            ctbody.selectable("refresh");
        }
    }

    function createUBlock(id, username, created){
        var tr = document.createElement('tr');
        var uId = document.createElement('td');
        var uName = document.createElement('td');
        var uDate = document.createElement('td');

        uId.setAttribute("class", "id");
        uId.setAttribute("style", "display:none");
        uName.setAttribute("class", "uName");

        uId.innerText = id;
        uName.innerText = username;
        uDate.innerText = formatDate(created);

        tr.appendChild(uId);
        tr.appendChild(uName);
        tr.appendChild(uDate);
        utbody[0].appendChild(tr);
    }

    function createCBlock(id, room, user_id, from, text, time){
        var tr = document.createElement('tr');
        var cId = document.createElement('td');
        var cRoom = document.createElement('td');
        var cUser_id = document.createElement('td');
        var cFrom = document.createElement('td');
        var cText = document.createElement('td');
        var cTime = document.createElement('td');

        cId.setAttribute("class", "id");
        cId.setAttribute("style", "display:none");
        cUser_id.setAttribute("class", "fId");
        cUser_id.setAttribute("style", "display:none");
        cRoom.setAttribute("style", "width:200px");
        cRoom.setAttribute("class", "cRoom");
        cFrom.setAttribute("class", "cFrom");

        cId.innerText = id;
        cRoom.innerText = room;
        cUser_id.innerText = user_id;
        cFrom.innerText = from;
        cText.innerText = text;
        cTime.innerText = formatDate(time);

        tr.appendChild(cId);
        tr.appendChild(cRoom);
        tr.appendChild(cUser_id);
        tr.appendChild(cFrom);
        tr.appendChild(cText);
        tr.appendChild(cTime);
        ctbody[0].appendChild(tr);
    }

    function formatDate(date) {
        var time = new Date(date);
        var min = time.getMinutes();
        if ( min < 10 ) min = '0' + min;
        var hh = time.getHours();
        if ( hh < 10 ) hh = '0' + hh;
        var dd = time.getDate();
        if ( dd < 10 ) dd = '0' + dd;
        var mm = time.getMonth()+1;
        if ( mm < 10 ) mm = '0' + mm;
        var yy = time.getFullYear() % 100;
        if ( yy < 10 ) yy = '0' + yy;
        return hh+':'+min+' '+dd+'/'+mm+'/'+yy;
    }

    function showError(err){
        console.log('Error: ' + err);
    }

    $('#ubutton').click(function(){
        makePostRequest({request:"getAllUsers"}, function(err, data){
            if (err) showError(err);
            else {
                utbody.empty();
                parseUsers(data);
            }
        });
    });

    $('#cbutton').click(function(){
        if(allComments.length != 0){
            ctbody.empty();
            parseComments(allComments);
        }
        else makePostRequest({request:"getAllComments"}, function(err, data){
            if (err) showError(err);
            else {
                allComments = data;
                ctbody.empty();
                parseComments(data);
            }
        });
    });

    $('#rem_ubutton').click(function(){
        getSelected("users", function(ids, selectedTrs){
            var data = {
              request: 'removeUsers',
              ids: ids
            };
            makePostRequest(data, function(err){
                if (err) showError(err);
                else removeSelected(selectedTrs);
            });
        });
    });

    $('#rem_cbutton').click(function(){
        getSelected("comments", function(ids, selectedTrs){
            var data = {
                request: 'removeComments',
                ids: ids
            };
            makePostRequest(data, function(err){
                if (err) showError(err);
                else removeSelected(selectedTrs);
            });
        });
    });

    $('#rem_sbutton').click(function(){
        getSelected("sockets", function(sockets, selectedTrs){
            socket.emit('killSockets', {'sockets': sockets});
            removeSelected(selectedTrs);
        });
    });

    function getSelected(target, callback){
        var Arr = [];
        var selectedTrs = [];
        if(target=="users"){
            selectedTrs = utbody.children('.ui-selected');
        }
        if(target=="comments"){
            selectedTrs = ctbody.children('.ui-selected');
        }
        if(target=="sockets"){
            selectedTrs = stbody.children('.ui-selected');
        }
        var selectedTds = selectedTrs.children('.id');
        for (var i=0; i<selectedTds.length; i++){
            Arr.push(selectedTds[i].innerText);
            console.log(selectedTds[i]);
        }
        callback (Arr, selectedTrs);
    }

    function removeSelected(selectedTrs){
        selectedTrs.remove();
    }

});