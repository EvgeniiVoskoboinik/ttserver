$(document).ready(function(){

    var socket
    , isConnected = false
    , isAuth = false
    , commentsLimit = 30
    , anonymously = false
    , tt_info_bar_timeout
    , check_con_timeout
    , comment_notification = false
    , lock = 2 // 1 - ready, 2 - lock
    , response_to_id = null
    , send_lock = false;

    var wrap =  $("#tt_wrap")
    , main = $("#tt_main")
    , tt_send = $('#tt_send')
    , authRegForm = $('#tt_auth_reg_forms')
    , authCloseButton = $('#tt_auth_close_button')
    , response_to_block = $('#tt_response_to_block')
    , response_button = $('#tt_response_button')
    , spam_button = $('#tt_spam_button')
    , auth_wrap = $('#tt_auth_wrap');


    main.tinyscrollbar();
    var scroll = main.data("plugin_tinyscrollbar");

    $("#tt_anonym_checkbox").change(function(){
        tt_send.focus();
        anonymously = this.checked;
     });

    $(document).tooltip({
        items:'.tt_response_to',
        position: { my: "left+15 top", at: "right center" },
        content:function(callback) {
            var el = $(this);
            var TTtmr = setTimeout( function() {
                socket.emit('one comment', {id: el.attr('res_to')}, function(err, comment){
                    if (!err){
                        createResponseBlock(comment[0], function(data){
                            return callback(data);
                        });
                    }
                });
            }, 500);
            el.mouseleave( function() { clearTimeout(TTtmr); } );
        }
    });

    function createResponseBlock(comment, callback){
        var data =
            '<div class="tt_response">' +
                '<div class="tt_response_nickname">' +
                    comment.from +
                '</div>' +
                '<div class="tt_response_time">' +
                    formatDate(comment.time) +
                '</div>' +
                '<div class="tt_clear"></div>' +
                '<div class="tt_response_text">' +
                    comment.text +
                '</div>' +
            '</div>';
        callback(data);
    }

    function makePostRequest(){
        var timerId;
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.open("POST", "/checkin", true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if(xhr.status == 200) {
                    clearTimeout(timerId);
                    connect();
                }
                else
                    timerId = setTimeout(makePostRequest, 5000);
            }
        };
        xhr.send();
    }

    function subscribeRoom(){
        socket.emit ('subscribe', 'tt');
    }

    function getCommentsFromDb(id){
        id = id || '000000000000000000000000';

        socket.emit("comments from db", {
                tabUrl: 'tt'
                , id: id
                , newer: true
            },
            function(err, comments){
                if (!err && comments.length != 0){

                    if (comments.length == commentsLimit)
                        lock = 1;

                    comments = comments.reverse();
                    parseNewComments(comments);
                }
            });
    }

    function parseNewComments(comments){
        for(var i = 0, len = comments.length; i<len; i++){
            var comment = comments[i];
            createCommentBlock(comment._id, comment.from, comment.text, comment.time, comment.response_to, true);
        }

        if (!comment_notification) {
            $('#tt_comment_notification').show();
            comment_notification = true;
        }
        scroll.update('bottom');
    }

    function getOlderCommentsFromDb(cid, callback){

        socket.emit("comments from db", {
                tabUrl: 'tt'
                , id: cid
                , newer: false
            },
            function(err, comments){
                if (!err && comments.length != 0){

                    if (comments.length >= commentsLimit)
                        lock = 1;

                    callback(comments);
                }
            });
    }

    wrap.draggable({
        handle:"#tt_header"
        , cancel:"#tt_close_button,#tt_minimize_button,#tt_restore_button"
        , scroll: false
    });

    wrap.resizable({
        alsoResize: "#tt_main"
        , handles: "s, w, sw"
        , minHeight: 245
        , minWidth: 133
    });
    wrap.resizable("disable");

    wrap.resizable({
        resize: function() {
            scroll.update('relative');
            wrap.css("height", "");
            wrap.css("position", "");
        }
    });

    $("#tt_minimize_button").click(function() {
        response_to_block.hide();
        $("#tt_main,#tt_send,#tt_footer,#tt_minimize_button").hide();
        $("#tt_restore_button").show();
        wrap.css("height", "");
        wrap.resizable("disable");
    });

    $("#tt_restore_button").click(function() {
        response_to_block.show();
        $("#tt_main,#tt_send,#tt_footer,#tt_minimize_button").show();
        $(this).hide();
        wrap.resizable("enable");
        scroll.update('bottom');
        tt_send.focus();
    });

    response_button.click(function(){
        var selectedComment = $('.tt_selected_comment');
        response_to_id = selectedComment.attr('id');
        tt_send.focus();
        response_to_block.html('To: ' + $('.tt_selected_comment .tt_nickname').text() + '<span>  (X)</span>')
                         .addClass('tt_response_to_block_hover');
        selectedComment.removeClass('tt_selected_comment');
        response_button.css('display', 'none');
        spam_button.css('display', 'none');
    });

    spam_button.click(function(){
        if(!isConnected)
            showErrNotification('Отсутствует подключение');
        else{
            var selectedComment = $('.tt_selected_comment');
            socket.emit('spam', {id: selectedComment.attr('id')});
            selectedComment.removeClass('tt_selected_comment');
            response_button.css('display', 'none');
            spam_button.css('display', 'none');
            showPositiveNotification('Жалоба отправлена');
        }
    });

    response_to_block.click(function(){
        response_to_id = null;
        response_to_block.text('').removeClass('tt_response_to_block_hover');
    });

    $('#tt_in_out').click(function(){
        if (!isAuth) showAuthRegForm();
        else logOut();
    });

    tt_send.keyup(function(e){ check_charcount(e); });

    tt_send.keydown(function(e){
        if (e.which == 13 && (e.ctrlKey == false)){
            e.preventDefault();
            checkAuth();
        }
        else
            check_charcount(e);
    });

    function check_charcount(e){
        if(e.which != 8 && tt_send.text().length > 450)
            e.preventDefault();
    }

    main.bind("move", function(){
        if (scroll.contentPosition == 0 && lock == 1){
            lock = 2;

            getOlderCommentsFromDb($('.tt_comment').attr('id'), function(comments){

                var contentSize1 = scroll.contentSize;

                for(var i = 0, len = comments.length; i<len; i++){
                    var comment = comments[i];
                    createCommentBlock(comment._id, comment.from, comment.text, comment.time, comment.response_to, false);
                }

                scroll.update();

                var contentSize2 = scroll.contentSize;

                scroll.update(contentSize2 - contentSize1);
            });
        }
    });

    function checkRegForm(login, pass1, pass2, callback){
        var pass_regexp =  /^[A-z0-9_-]{1,}$/i;
        var login_regexp =  /^[A-z0-9_-]{1,}$/i;

        if(login == "" || pass1 == "" || pass2 == "")
            return callback("Не все поля заполнены");

        else if(login.length > 20 || login.length < 3)
            return callback("Допустимая длина имени 3-20 символов");

        else if(pass1.length < 5)
            return callback("Пароль не должен быть короче 5 символов");

        else if(!pass_regexp.test(pass1) || !login_regexp.test(login))
            return callback("Допускаются только латинские буквы, цифры, нижнее подчеркивание, тире");

        else if (pass1 != pass2)
            return callback("Пароли не совпадают");

        return callback();
    }

    authRegForm.accordion({
        heightStyle: "content",
        active: 0
    });

    $("#tt_auth_form").submit(function (e) {
        e.preventDefault();
        var login = $("#tt_auth_username").val().trim()
            , pass = $("#tt_auth_password").val();

        if(login == "" || pass == "")
            showAuthErrNotification("Не все поля заполнены");
        else{
            var formData = {
                "username":login
                , "password":pass
            };
            makeAuth(formData);
        }
    });

    $("#tt_reg_form").submit(function (e) {
        e.preventDefault();
        var login = $("#tt_reg_username").val().trim()
            , pass1 = $("#tt_reg_password1").val()
            , pass2 = $("#tt_reg_password2").val();
        checkRegForm(login, pass1, pass2, function(err){
            if(err)
                showRegErrNotification(err);
            else{
                var formData = {
                    "username":login
                    , "password1":pass1
                    , "password2":pass2
                };
                makeReg(formData);
            }
        });
    });

    function createCommentBlock(id, from, text, time, res_to, after){

        var table = document.createElement('table')
            , tbody = document.createElement('tbody')
            , trUp = document.createElement('tr')
            , trDown = document.createElement('tr')
            , nickname = document.createElement('td')
            , response_to = document.createElement('td')
            , comment_time = document.createElement('td')
            , comment_text = document.createElement('td')
            , comment = document.createElement('div')
            , root = document.getElementsByClassName('tt_overview');

        nickname.setAttribute("class", "tt_nickname");
        nickname.setAttribute("style", "font-size:11px");
        comment_time.setAttribute("class", "tt_comment_time");
        comment_time.setAttribute("style", "font-size:10px");
        comment_text.setAttribute("class", "tt_comment_text");
        comment_text.setAttribute("colspan", 3);
        comment_text.setAttribute("style", "font-size:11px");
        comment.setAttribute("class", "tt_comment");
        comment.setAttribute("id", id);

        nickname.textContent = from;
        comment_time.textContent = formatDate(time);
        comment_text.textContent = text;

        if(res_to){
            response_to.setAttribute("res_to", res_to);
            response_to.setAttribute("class", 'tt_response_to');
        }

        trUp.appendChild(nickname);
        trUp.appendChild(response_to);
        trUp.appendChild(comment_time);
        trDown.appendChild(comment_text);
        tbody.appendChild(trUp);
        tbody.appendChild(trDown);
        table.appendChild(tbody);
        comment.appendChild(table);

        if (after)
            root[0].appendChild(comment);
        else
            $('.tt_overview').prepend(comment);

        $('#'+id).on('click', function(e){
            var el = $(this);

            if(e.target.className == 'tt_comment_text'){
                if(el.hasClass("tt_selected_comment")){
                    el.removeClass("tt_selected_comment");
                    response_button.css('display', 'none');
                    spam_button.css('display', 'none');
                }
                else{
                    $('.tt_selected_comment').removeClass('tt_selected_comment');
                    el.addClass("tt_selected_comment");
                    response_button.css('display', 'block');
                    spam_button.css('display', 'block');
                }
            }
        });
    }

    function formatDate(date) {
        var now = new Date()
            , time = new Date(date)
            , min = time.getMinutes()
            , hh = time.getHours()
            , dd = time.getDate()
            , mm = time.getMonth()+1
            , yy = time.getFullYear() % 100;

        var today = new Date(now.getFullYear(), now.getMonth(), now.getDate()).valueOf()
            , time1 = new Date(time.getFullYear(), time.getMonth(), time.getDate()).valueOf();

        if(today == time1){
            if ( min < 10 ) min = '0' + min;
            return hh+':'+min;
        }

        if ( mm < 10 ) mm = '0' + mm;
        return dd+'.'+mm+'.'+yy;
    }

    $.fn.getPreText = function () {
        var ce = $("<pre />").html(this.html())
            , nAgt = navigator.userAgent;

        if (nAgt.indexOf("Chrome") != -1)
            ce.find("div").replaceWith(function() { if (this.innerText != '\n') return "\n" + this.innerText; });

        if (nAgt.indexOf("Firefox") != -1 || nAgt.indexOf("Opera") != -1)
            ce.find("br").replaceWith("\n");

        return ce.text();
    };

    function logOut(){
        socket.emit('logout', function(err){
            if(err) showErrNotification(err);
            else {
                isAuth=false;
                $('#tt_greeting').text('');
                $('#tt_in_out').text('Войти');
                showPositiveNotification('Вы успешно вышли');
            }
        })
    }

    function logIn(username){
        $('#tt_greeting').text('Привет, ' + username);
        $('#tt_in_out').text('Выйти');
    }

    function makeAuth(data){
        socket.emit('makeAuth', data, function(err){
            if (!err){
                isAuth = true;
                logIn(data.username);
                showPositiveNotification("Авторизация прошла успешно");
                auth_wrap.css('display', 'none');
            }
            else
                showErrNotification(err);
        });
    }

    function makeReg(data){
        socket.emit('makeReg', data, function(err){
            if(!err){
                showPositiveNotification("Вы успешно зарегистрировались");
                $("#tt_reg_form")[0].reset();
                authRegForm.accordion( "option", "active", 0 );
            }
            else
                showErrNotification(err);
        });
    }

    function sendComment(){
        if(!send_lock){
            send_lock = true;
        }
        var comment = tt_send.getPreText().replace(/\xA0+/g, '').replace(/[ ]{2,}/g, ' ').replace(/[\n]{2,}/g, '\n').trim(); //ужас
        if(comment != ''){
            var tt_info_bar = $('#tt_info_bar');

            tt_info_bar.css("background-color", "#dddddd");
            clearTimeout(tt_info_bar_timeout);
            tt_info_bar_timeout = setTimeout(function(){
                tt_info_bar.html('Отправляю...');
                check_con_timeout = setTimeout(function(){
                    showErrNotification('Проверьте подключение к интернету');
                    send_lock = false;
                }, 10000)
            }, 1000);

            socket.emit ('comment', {
                    anonymously: anonymously
                    , comment: comment
                    , response_to: response_to_id
                    , tabUrl: 'tt'
                },
                function(err, data){
                    clearTimeout(tt_info_bar_timeout);
                    clearTimeout(check_con_timeout);
                    tt_info_bar.empty();

                    if(!err){
                        parseNewComments(data.comments);
                        tt_send.empty();
                        response_to_id = null;
                        response_to_block.text('').removeClass('tt_response_to_block_hover');
                    }
                    else
                        showErrNotification(err);
                    send_lock = false;
                });
        }
    }

    function checkAuth(){
        if (!isConnected)
            showErrNotification('Соединение с сервером отсутствует');
        else if (!isAuth) {
            showErrNotification('Для этого действия необходимо авторизоваться');
            showAuthRegForm();
        }
        else
            sendComment();
    }

        function showAuthRegForm(){
            auth_wrap.css('display', 'block');

            $(document).click(function (e) {
                if ($(e.target).closest('#tt_auth_reg_forms, #tt_wrap').length == 0) {
                    auth_wrap.css('display', 'none');
                    $(this).off('click');
                }
            });
        }

    function showErrNotification(message){
        showNotification(message, '#ffefe8');
    }

    function showPositiveNotification(message){
        showNotification(message, '#dfffe8');
    }

    function showNotification(message, background){
        var tt_info_bar = $('#tt_info_bar');
        tt_info_bar.css("background-color", background);
        clearTimeout(tt_info_bar_timeout);
        tt_info_bar.html(message);
        tt_info_bar_timeout = setTimeout(function(){ tt_info_bar.empty() },5000);
    }

    function showAuthErrNotification(message){
        var e = $('#tt_auth_form_notification');
        var t = setTimeout(function(){ e.empty() },5000); //TODO: clearTimeout
        e.text(message);
    }

    function showRegErrNotification(message){
        var e = $('#tt_reg_form_notification');
        var t = setTimeout(function(){ e.empty() },5000); //TODO: clearTimeout
        e.text(message);
    }

    function connect(){
        socket = io.connect('/');
        socket
            .on('connect', function(){
                isConnected = true;
                var lastCommentId = $('.tt_comment').last().attr('id');
                getCommentsFromDb(lastCommentId);
                subscribeRoom();
            })

            .on('auth', function(data){
                if (isAuth= data.isAuth) logIn(data.username);
            })

            .on('comment', function(data){
                parseNewComments(data.comments);
            })

            .on('logout', function(){
                isAuth = false;
            })

            .on('disconnect', function(){
                isConnected = false;
            });
    }

    makePostRequest();

});