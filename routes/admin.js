var User = require('models/user').User,
    Comment = require('models/comment').Comment,
    HttpError = require('error').HttpError;

exports.get = function(req, res) {
    var uid = req.session.user;

    if(uid){
        User.getUser(uid, function(err, user){
            if (!err && user.status === 1) res.render('admin');
            else res.sendHttpError(new HttpError(404));
        });
    }
    else res.sendHttpError(new HttpError(404));
};

exports.post = function(req, res) {
    switch (req.body.request){
        case "getAllUsers":
            User.getAllUsers(function(err, users) {
                if (!err) res.send(users);
                else res.send(err);
            });
            break;
        case "getAllComments":
            Comment.getAllComments(function(err, comments){
                if (!err) res.send(comments);
                else res.send(err);
            });
            break;
        case "removeUsers":
            User.removeUsers(req.body.ids, function(err){
                if (!err) res.send({});
                else res.send(err);
            });
            break;
        case "removeComments":
            Comment.removeComments(req.body.ids, function(err){
                if (!err) res.send({});
                else res.send(err);
            });
            break;
        default:
            res.send("Bad request");
    }
};