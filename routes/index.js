var session = require('lib/session');

module.exports = function(app) {
  app.get('/ttyper', require('./frontpage').get);

  //app.get('/robots.txt', require('./robots').get);

  //app.get('/admin', session, require('./admin').get);

  //app.post('/admin', require('./admin').post);

  app.post('/checkin', session, require('./checkin').post);

  //app.get('/googled182afc2ae105ce3.html', require('./googled.js').get);
};