var config = require('config')
    , async = require('async')
    , cookie = require('cookie')
    , sessionStore = require('lib/sessionStore')
    , cookieParser = require('cookie-parser')
    , User = require('models/user').User
    , Comment = require('models/comment').Comment
    , activeSockets = require('middleware/activeSockets');
    //var util = require('util');

function getSid(handshake) {
    handshake.cookies = cookie.parse(handshake.headers.cookie || '');
    var sidCookie = handshake.cookies[config.get('session.key')];

    if (sidCookie) {
        return cookieParser.signedCookie(sidCookie, config.get('session.secret'));
    }
}

function loadSession(sid, callback) {
    sessionStore.load(sid, function(err, session) {
        if (err) return callback(err);
        if (arguments.length === 0)
            // no arguments => no session
            return callback('no session');
        return callback(null, session);
    });
}

function loadUser(session, callback) {
    if (!session.user)
        callback(null, null, null);
    else
        User.findById(session.user, function(err, user) {
            if (err)
                return callback(err);
            if (!user)//user was deleted
                return callback(null, null, null);
            return callback(null, user, session.id);
        });
}

module.exports = function(server) {
    var io = require('socket.io')(server, {
        'pingInterval':parseInt(process.env.P_INTERVAL),
        'pingTimeout': parseInt(process.env.P_TIMEOUT)
    });

    //var redis = require('socket.io-redis')
    //    , rtg   = require("url").parse(process.env.REDIS_URL);
    //
    //io.adapter(redis({host: rtg.hostname, port: rtg.port}));

    io.use(function(socket, next) {

        var handshake = socket.request;
        //console.log(util.inspect(handshake));
        async.waterfall([
            function(callback) {
                loadSession(getSid(handshake), callback);
            },
            function(session, callback) {
                if (!session) {
                    console.log ('no session repeat'); //will never fire
                    callback(403);
                }
                else{
                    handshake.session = session;
                    loadUser(session, callback);
                }
            },
            function(user, sessionId, callback) {
                if (!user) {
                    socket.emit('auth', {'isAuth':false});
                    var username = 'hz';
                }
                else {
                    username = user.username;
                    handshake.user = {
                        username: username,
                        _id: user._id
                    };
                    socket.emit('auth', {'isAuth':true, 'username':username});
                }

                activeSockets.addSocket(socket.id, socket.handshake.address, username, sessionId);

                callback();
            }
        ], function(err) {
                return next(err);
        });
    });

    io.on('connection', function(socket) {
        //console.log(io.engine.clientsCount);

        socket
            .on('subscribe', function(room){
                if (typeof socket.request.user === 'undefined') return;
                socket.join(room);
            })

            .on('comment', function(data, callback) {
                if (typeof socket.request.user === 'undefined' || typeof (callback) == "undefined") return;

                var comment = data.comment.trim();
                var room = data.tabUrl;

                if (comment === '' || room === '') return;

                var response_to = data.response_to;

                if(data.anonymously)
                    var from = 'Anonym';
                else
                    from = socket.request.user.username;

                Comment.saveComment(room, socket.request.user._id, from, comment, response_to, function(err, saved_comment){
                    if(err)
                        return callback('Sending error');

                    var res = {
                        'tabUrl': room,
                        'comments':[{
                            'from': from,
                            'text': comment,
                            'response_to': response_to,
                            '_id': saved_comment._id,
                            'time': saved_comment.time
                        }]
                    };

                    socket.broadcast.to(room).emit('comment', res);

                    return callback(null, res);
                });
            })

            .on('one comment', function(data, callback) {
                if (typeof (callback) === "undefined") return;

                Comment.getCommentbyId(data.id, function(err, comment){
                    if(err)
                        return callback('Error');

                    return callback(null, comment);
                });
            })

            .on('comments from db', function (data, callback){
                if (typeof (callback) === "undefined") return;
                var room = data.tabUrl;

                if (room !== ''){
                    Comment.getComments(room, data.id, data.newer, function(err, comments) {
                        if (err)
                            return callback(err);
                        else
                            return callback(null, comments);
                    });
                }
            })

            .on('spam', function(data){
                console.log('SPAM: ' + data.id);
                //TODO:add note to spam.log
            })

            .on('makeReg', function(data, callback){
                if (typeof (callback) === "undefined") return;

                var login_regexp =  /^[A-z0-9_-]{1,}$/i;
                var login = data.username;
                var pass = data.password1;

                if(login === "" ||
                    pass === "" ||
                    login.length > 20 ||
                    login.length < 3 ||
                    !login_regexp.test(login)
                )
                    callback("Error");

                User.registration(login, pass, function(err, user) {
                    return callback (err);
                });
            })

            .on('makeAuth', function(data, callback){
                if (typeof (callback) === "undefined") return;

                var login = data.username;
                var pass = data.password;

                if (login === "" || pass === "")
                    callback("Error");

                User.authorize(login, pass, function(err, user) {
                    if (err)
                        return callback (err);

                    sessionStore.load(getSid(socket.request), function(err, session) {//проверка err
                        if (arguments.length != 0) {
                            session.user = user._id;//Записываем в бд Id юзера для этой сессии
                            session.save();
                            socket.request.user = user; //Записываем в сокет информацию о юзере !!!перенес сюда, т.к. ф-я асинхронная
                            activeSockets.addSocket(socket.id, socket.handshake.address, user.username, session.id);
                            return callback();
                        }
                        return callback('Bad session');
                    });
                });
            })

            .on('logout', function(callback){
                var sessionId = activeSockets.getSessionId(socket.id);

                sessionStore.load(sessionId, function(err, session){
                    if(!err){
                        delete session.user;
                        session.save();
                        delete socket.request.user;
                        activeSockets.addSocket(socket.id, socket.handshake.address, 'hz', session.id);
                        callback();
                    }
                });
            })

            .on('disconnect', function(){
                activeSockets.deleteSocket(socket.id);
            })

            .on('killSockets', function(data){
                var sockets = data.sockets;
                for (var socket in sockets){

                    if (io.sockets.connected[sockets[socket]]) {
                        var sessionId = activeSockets.getSessionId(sockets[socket]);
                        if(sessionId != null)
                            sessionStore.destroy(sessionId, function(){
                                io.sockets.connected[sockets[socket]].disconnect();
                            });
                           else {
                            io.sockets.connected[sockets[socket]].disconnect();
                        }
                    }
                }

            })
    });
    return io;
};